<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Employee;

Route::get('/employees', 'EmployeeController@index');
Route::post('/employees', 'EmployeeController@Store');
Route::put('/employees/{id}','EmployeeController@update');