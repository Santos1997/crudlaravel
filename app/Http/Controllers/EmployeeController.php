<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::get();
        return $employees;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|max:50|email|unique:employees',
            'age' => 'required|numeric'
        ]);

        $employee = new Employee();

        $employee->name = $request->input('name');
        $employee->last_name = $request->input('last_name');
        $employee->email = $request->input('email');
        $employee->age = $request->input('age');

        $employee->save();

        return $employee;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Employee $employee, int $id)
    {
        return $employee;
        $employeeFound = Employee::findOrFail($id);
        $employeeFound->name = $employee->name;
        $employeeFound->last_name = $employee->last_name;
        $employeeFound->email = $employee->email;
        $employeeFound->age = $employee->age;
        $employeeFound->save();
        return $employeeFound;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
